#include <amxmodx>
#include <filestats>

#define PLUGIN "Usuwanie logow"
#define VERSION "0.2.0"
#define AUTHOR "Sebul"

#pragma semicolon 1

enum eFileData {
	FDNazwa[64],
	FDIleDni
};

const UL_DZIEN = 60*60*24;
new const g_szOgraniczenie[] = "10";

new const g_sciezki[][eFileData] = {
	{"logs", 30},
	{"addons/amxmodx/logs", 10},
	{"addons/amxmodx/logs/inne", 20},
	{"addons/amxmodx/logs/diablo", 20},
	{"addons/amxmodx/logs/netchan", 10},
	{"addons/amxmodx/logs/voice", 10}
};

new const g_nazwy[][eFileData] = {
	{"L", 20},
	{"error_", 30},
	{"Say_", 20},
	{"IPLog_", 30},
	{"diablo_log_", 20},
	{"diablo_debug_", 30},
	{"Net_", 10},
	{"vfam_", 10}
};

new g_szIle[3], g_iIle;

public plugin_init() {
	register_plugin(PLUGIN, VERSION, AUTHOR);
}

public plugin_cfg() {
	get_localinfo("DelLogs", g_szIle, 2);
	g_iIle = str_to_num(g_szIle);

	if(g_iIle > 0) return;

	g_iIle = str_to_num(g_szOgraniczenie);
	set_localinfo("DelLogs", g_szOgraniczenie);

	for(new j,i=0,ile=sizeof(g_sciezki),ile2=sizeof(g_nazwy),bool:ok,st=get_systime(),dh,szFile[31],lenF,szFile2[64],timeF; i<ile; ++i) {
		dh = open_dir(g_sciezki[i][FDNazwa], szFile, 31);
		if(!dh) continue;
		do {
			if(szFile[0] == '.' || (lenF = strlen(szFile)) < 5 || !equali(szFile[lenF-4], ".log")) continue;
			ok = false;
			for(j=0; j<ile2; ++j) {
				if(equal(szFile, g_nazwy[j][FDNazwa], strlen(g_nazwy[j][FDNazwa]))) {
					ok = true;
					break;
				}
			}
			if(!ok) continue;
			formatex(szFile2, 63, "%s/%s", g_sciezki[i][FDNazwa], szFile);
			if((timeF = filemtime(szFile2)) < st-UL_DZIEN*g_sciezki[i][FDIleDni] && timeF < st-UL_DZIEN*g_nazwy[j][FDIleDni]) delete_file(szFile2);
		}
		while(next_file(dh, szFile, 31));
		close_dir(dh);
	}
}

public plugin_end() {
	num_to_str(g_iIle-1, g_szIle, 2);
	set_localinfo("DelLogs", g_szIle);
}
